
PNGREADER=open
DOT=dot

build :
	dune build

test :
	dune runtest

graph : build
	./_build/default/src/LexerMain.exe > toto.temp
	${DOT} -Tpng toto.temp > data/nfa.png
	rm toto.temp
	${PNGREADER} data/nfa.png

clean :
	dune clean
	rm -f data/nfa.png
