
module V = struct
  type t = Int.t

  let equal = Int.equal
  let compare = Int.compare
  let hash = Hashtbl.hash
end

module E = struct
  type t = char option

  let compare : t -> t -> int = fun label1 label2 ->
    match label1, label2 with
    | None, None       ->  0
    | None, Some _     -> +1
    | Some _, None     -> -1
    | Some l1, Some l2 -> Char.compare l1 l2

  let default : t = None
end

module G = Graph.Persistent.Digraph.ConcreteLabeled(V)(E)
