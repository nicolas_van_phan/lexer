(*
  Adjacency-list implementation of labeled digraph

  This module implements a graph structure using adjacency-list of directed-graph with labeled edges.
  The graph structure is a map binding each source vertex with its list of adjacences,
  An adjacence of a vertex is an output transition from this vertex,
  it is composed of a destination vertex along with an edge label.
  So, the list of adjacence of a vertex is the list of all output transitions from that vertex.

  This module doesn't allow duplicate edge (same source vertex, destination vertex and edge label),
  to enforce this, it needs comparable vertex and edge types, hence the [compare] requirement in below modules,
  and the adjacency-list is actually an adjacency-set to prevent duplicate elements.

  TODO : Vertex labels are hardcoded as [int] here. Make it more generic.
*)
module type Vertex = sig
  type t
  val compare : t -> t -> int
  val to_string : t -> string
end

module type EdgeLabel = sig
  type t
  val compare : t -> t -> int
  val to_string_opt : t -> string option
end

module Make (V : Vertex) (E : EdgeLabel) : sig

  module GraphMap : module type of Map.Make (V)

  type vertex = V.t
  type edge = E.t

  module Adj : sig
    type t = vertex * edge
    val compare : t -> t -> int
    val dump_dot : vertex -> t -> string
  end

  (* Defined as a Set instead of a List, in order to prevent duplicate adjacencies *)
  module AdjSet : sig
    include module type of Set.Make (Adj)

    (* Returns the union of the given [adj] singleton and [adj_set],
      for either elements, [None] is treated as the empty set *)
    val update : Adj.t option -> t option -> t option
    val dump_dot : V.t -> t -> string
  end

  type t = AdjSet.t GraphMap.t
  type nfa = t

  (* Returns an empty graph *)
  val empty : t

  (* Adds an edge to the graph.
     If the source or destination vertex is not already present, it will be added to the graph *)
  val add_edge : (V.t * V.t * E.t) -> t -> t

  (* Merges two graphs together, returns a graph containing all vertices and edge from the two merging graphs
     If the two mergees both have the same vertex [42] (the same = identical vertices in the sense of [Vertex.compare]),
     there are two policies here :
      1. We could consider that the two graphs are distinct and all their vertices are different, and rename them in the merge graph.
      For example, here we would rename [42] from graph 1 and [42] from graph 2 into two different integers.
      2. We consider they are one and the same vertex in the merge graph.
      This means that we don't rename anything, and we merge the two adjacency lists of [42] of the two mergees graphs.
    We chose 2. here
  *)
  val merge : t -> t -> t
  (* Returns a string representation of the graph in the [.dot] format *)
  val dump_dot : t -> string
  (* Prints the [.dot] string returned by [dump_dot] *)
  val print_dot : t -> unit
end
