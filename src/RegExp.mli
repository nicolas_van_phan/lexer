(*
  Regular Expression
*)
type t =
  | Atom   of char   (* d *)
  | Concat of t * t  (* de *)
  | Union  of t * t  (* de | fr *)
  | Kleene of t      (* d* *)
  | Pos    of t      (* d+ *)

type regexp = t

(* Builds a regexp type from a string regexp expression. *)
(* val of_string : string -> regexp *)

(* Prints the string returned by [dump] *)
val print : regexp -> unit
