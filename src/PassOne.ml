(*
Conversion from Regexp to Subgraph
*)

let subgraph_of_regexp : RegExp.t -> Subgraph.t =
  fun re ->
    let rec aux : int -> RegExp.t -> Subgraph.t * int =
      fun n re -> match re with
      | Atom c ->

          let name = Char.escaped c in
          let i, o, n = n, n+1, n+2 in
          let subgraphs = [] in
          let edges = [ (i, o, Some c) ] in
          { name ; subgraphs ; edges ; i ; o }, n

      | Concat (re1, re2) ->

          let g1, n = aux n re1 in
          let g2, n = aux n re2 in
          let name = g1.name ^ g2.name in
          let i, o, n = g1.i, g2.o, n in
          let subgraphs = [ g1 ; g2 ] in
          let edges = [ (g1.o, g2.i, None) ] in
          { name ; subgraphs ; edges ; i ; o }, n

      | Union (re1, re2) ->

          let g1, n = aux n re1 in
          let g2, n = aux n re2 in
          let name = "( " ^ g1.name ^ " | " ^ g2.name ^ " )" in
          let i, o, n = n, n+1, n+2 in
          let subgraphs = [ g1 ; g2 ] in
          let edges = [
            (i    , g1.i , None);
            (i    , g2.i , None);
            (g1.o , o    , None);
            (g2.o , o    , None);
          ] in
          { name ; subgraphs ; edges ; i ; o }, n

      | Kleene re1 ->

          let g1, n = aux n re1 in
          let name = "[" ^ g1.name ^ "]*" in
          let i, o, n = n, n+1, n+2 in
          let subgraphs = [ g1 ] in
          let edges = [
            (i    , g1.i , None);
            (g1.o , o    , None);
            (g1.o , g1.i , None);
            (i    , o    , None);
          ] in
          { name ; subgraphs ; edges ; i ; o }, n

      | Pos re1 ->

          let g1, n = aux n re1 in
          let name = "[" ^ g1.name ^ "]+" in
          let i, o, n = n, n+1, n+2 in
          let subgraphs = [ g1 ] in
          let edges = [
            (i    , g1.i , None);
            (g1.o , o    , None);
            (g1.o , g1.i , None);
          ] in
          { name ; subgraphs ; edges ; i ; o }, n

    in
    let (g, _) = aux 0 re in g

