(* 
  There are two structures used to represent the NFA.
  1. [module Subgraph] Nested subgraph with I/O :
    regexp is made of subexpressions made themselves of subexpressions,
    each subexpression is associated to a subgraph with an input node and an output node,
    and those subgraphs are nested within each other.
    This structure is convenient because it can be easily created from the tree regexp structure.
    Also, since it contains the hierarchy of subgraphs, it can be printed as a [.dot] graph
    with rectagles delimiting each subgraph (see [Subgraph.print]),
    which gives a much better view of the NFA than the plain graph with no hierarchy.

  2. (TODO) [module G] Adjacency-list graph :
    If the former structure is easy to build and print, it is not easy to search.
    We'll need a searchable NFA graph structure to implement the NFA->DFA converter,
    This is why we have this second representation.
   TODO : Add G using ocamlgraph

  The idea is that regexp are first converted to Subgraph (and Subgraph can be exported as [.dot])
  then Subgraph is converted to AdjGraph for later conversion into the DFA.

  Finally, the main [t] type for this module is the second representation [G.t],
  along with 2 additional elements (the start node and accepting node)   
*)

module V = Shared.V
module E = Shared.E

(* small-step approach *)
type t = {
  name : string;
  subgraphs : t list;
  edges : (V.t * V.t * E.t) list;
  i : V.t;
  o : V.t;
}
type subgraph = t

(* TODO : Labels are not printed in transitions because this streches the [.dot] output graph very long for some reason. Find a workaround *)
let rec print_subgraph ppf g =
  Format.fprintf ppf "subgraph \"cluster_%s\" @,{@[<v 2>@,label = \"%s\";@,%a@,%a@]@,}" g.name g.name print_edges g.edges print_subgraphs g.subgraphs
and print_edges ppf = function
  | [] -> Format.fprintf ppf ""
  | (v1, v2, _) :: t -> Format.fprintf ppf "@,%d -> %d;%a" v1 v2 print_edges t
and print_subgraphs ppf = function
  | [] -> Format.fprintf ppf ""
  | h :: t -> Format.fprintf ppf "@,%a%a" print_subgraph h print_subgraphs t

let print : t -> unit =
  fun sg ->
    Format.fprintf Format.std_formatter "digraph MyRegExp @,{@[<v 2>@,%d [shape=record];@,%d [shape=record];@,@,%a @]@,}" sg.i sg.o print_subgraph sg

