
type t =
  | Atom   of char   (* d *)
  | Concat of t * t  (* de *)
  | Union  of t * t  (* de | fr *)
  | Kleene of t      (* d* *)
  | Pos    of t      (* d+ *)

type regexp = t

let print : regexp -> unit =
  Format.set_margin 80;
  Format.set_max_indent 80;
  let rec pp ppf = function
    | Atom   c          -> Format.fprintf ppf "Atom %c" c
    | Concat (re1, re2) -> Format.fprintf ppf "Concat @[<v>@ %a @ %a@]" pp re1 pp re2
    | Union  (re1, re2) -> Format.fprintf ppf "Union @[<v>@ %a @ %a@]" pp re1 pp re2
    | Kleene re         -> Format.fprintf ppf "Kleene @[<v>@ %a@]" pp re
    | Pos    re         -> Format.fprintf ppf "Pos @[<v>@ %a@]" pp re
  in fun re -> pp Format.std_formatter re
