
open LibNfa

(*
   a | ( b* | cd+ )   
*)
let myre =
  let open RegExp in
    Union (
      Atom 'a'
    , Union (
        Kleene (
          Atom 'b'
        )
      ,
        Concat (
          Atom 'c'
        ,
          Pos (
            Atom 'd'
          )
        )
      )
    )

let () =
  (* Convert the above regexp to NFA and print it *)
  let nfa = PassOne.subgraph_of_regexp myre in
  Subgraph.print nfa
