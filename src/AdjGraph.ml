
module type Vertex = sig
  type t
  val compare : t -> t -> int
  val to_string : t -> string
end

module type EdgeLabel = sig
  type t
  val compare : t -> t -> int
  val to_string_opt : t -> string option
end

(* BEGINNING OF MODULE MAKE *)

module Make (V : Vertex) (E : EdgeLabel) = struct

  type vertex = V.t
  type edge = E.t

  let dump_transition_dot : V.t -> V.t -> E.t -> string =
    fun v1 v2 edge ->
      match E.to_string_opt edge with
      | Some label -> Printf.sprintf "%s -> %s [ label = \"%s\" ]\n" (V.to_string v1) (V.to_string v2) label
      | None -> Printf.sprintf "%s -> %s\n" (V.to_string v1) (V.to_string v2)

    (* ADJACENCY MODULE *)
  module Adj = struct
    type t = vertex * edge
    type adj = t
    let compare : adj -> adj -> int =
      fun (v1, e1) (v2, e2) ->
        let compare_vertices = V.compare v1 v2 in
        if compare_vertices = 0 then E.compare e1 e2 else compare_vertices

    let dump_dot : vertex -> t -> string =
      fun v1 (v2, edge) ->
        dump_transition_dot v1 v2 edge
  end (* of ADJACENCY MODULE*)

  (* ADJACENCY LIST MODULE *)
  (* Defined as a Set instead of a List, in order to prevent duplicate adjacencies *)
  module AdjSet = struct
    include Set.Make (Adj)

    (* Returns the union of the given [adj] singleton and [adj_set],
      for either elements, [None] is treated as the empty set *)
    let update : Adj.t option -> t option -> t option =
      fun adj_opt adj_set_opt ->
      let adj_set =
        match adj_set_opt with
        | None -> empty
        | Some adj_set -> adj_set
      in
      match adj_opt with
      | None -> Some adj_set
      | Some adj -> Some (add adj adj_set)

    let dump_dot : vertex -> t -> string =
      fun v adj_set ->
        fold (fun adj str -> str ^ Adj.dump_dot v adj) adj_set ""
  end
  module GraphMap = Map.Make (V)

  type t = AdjSet.t GraphMap.t
  type nfa = t
  let empty : nfa = GraphMap.empty

  let add_edge : (vertex * vertex * edge) -> nfa -> nfa =
    fun (v1, v2, label) g ->
      let adj = v2, label in
      let g = GraphMap.update v1 (AdjSet.update (Some adj)) g in
      let g = GraphMap.update v2 (AdjSet.update None) g in
      g

  let merge : t -> t -> t =
    let adj_list_merge : vertex -> AdjSet.t -> AdjSet.t -> AdjSet.t option =
      fun _key l1 l2 ->
        Some (AdjSet.union l1 l2)
    in fun g1 g2 ->
      GraphMap.union adj_list_merge g1 g2

  let dump_dot : nfa -> string =
    fun nfa ->
      let header = "digraph {\n" in
      let body = GraphMap.fold (fun key value accu -> accu ^ (AdjSet.dump_dot key value)) nfa "" in
      let footer = "}\n" in
      header ^ body ^ footer

  let print_dot : nfa -> unit = fun nfa -> print_endline (dump_dot nfa)

end (* of MAKE MODULE *)
