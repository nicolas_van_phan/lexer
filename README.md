
# Lexer

This is an OCaml implementation of a lexer (or at least parts of it...) as an exercise to better understand the principles of lexing.

The goal is to start from a string representation of a regexp and transform it into an deterministic finite automaton (DFA).
Following these intermediate steps :
1. string regexp
2. tree regexp
3. NFA
4. DFA